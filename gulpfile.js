var gulp = require('gulp');
var babel = require('gulp-babel');
var uglify = require('gulp-uglify');
var path = require('path');

/*  Variables  */
var compileFiles = [
  "src/*.js"
];

/*  Tasks  */

gulp.task('compile', () => {
  compileFiles.forEach(
    x => gulp.src(x)
            .pipe(babel())
            //.pipe(uglify())
            .pipe(gulp.dest(path.dirname(x.replace('src', 'lib'))))
  );
});

gulp.task('watch', ['compile'], () => {

  gulp.watch(compileFiles, (e) => {
    if(e.type === 'changed') {
      console.log('File ' + e.path + ' was modified. Compiling...');
      return gulp.src(e.path)
              .pipe(babel())
			  //.pipe(uglify())
              .pipe(gulp.dest(path.dirname(e.path.replace('src', 'lib'))));
    }
  });

});

gulp.task('default', ['compile']);
