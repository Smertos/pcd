'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _Exceptions = require('./Exceptions.js');

var _Exceptions2 = _interopRequireDefault(_Exceptions);

var _Download = require('./Download.js');

var _Download2 = _interopRequireDefault(_Download);

var _DownloadAll = require('./DownloadAll.js');

var _DownloadAll2 = _interopRequireDefault(_DownloadAll);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (typeof global.PubSub == 'undefined') global.PubSub = require('pubsub-js');

exports.default = {
  download: function download(URL, Path) {
    return new _Download2.default(URL, Path).start();
  } /*.catch((err) => console.log('Error: ' + err.message))*/
  , downloadAll: _DownloadAll2.default
};