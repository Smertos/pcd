'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = DownloadAll;

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _Download = require('./Download.js');

var _Download2 = _interopRequireDefault(_Download);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function DownloadAll(dArr, eventName, interval, concurrency) {

  var promises = dArr.map(function (el) {
    return new _Download2.default(el.URL, el.path);
  });
  var downloaded = 0;
  var sizeOverall = 0;

  return _bluebird2.default.map(promises, function (el) {
    return el.getSize();
  }, { concurrency: concurrency }).then(function () {
    promises.forEach(function (el) {
      PubSub.subscribe(el.URL + ':' + el.SavePath + '.progress', function (e, size) {
        if (typeof size != 'undefined') downloaded += Number(size);
      });

      sizeOverall += Number(el.Size);
    });

    var inter = setInterval(function () {
      return PubSub.publish(eventName + '.progress', { downloaded: downloaded, sizeOverall: sizeOverall });
    }, interval);

    PubSub.subscribe(eventName + '.downloaded', function () {
      return clearInterval(inter);
    });

    return _bluebird2.default.map(promises, function (el) {
      return el.start();
    }, { concurrency: concurrency }).then(function () {
      return PubSub.publish(eventName + '.downloaded');
    });
  });
};