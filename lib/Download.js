'use strict';

Object.defineProperty(exports, "__esModule", {
		value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _Exceptions = require('./Exceptions.js');

var _Exceptions2 = _interopRequireDefault(_Exceptions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var prequest = function prequest(d) {
		return new _bluebird2.default(function (res, rej) {
				return _request2.default.get(d.URL, function (err, data) {
						if (!err) {

								PubSub.publish(d.URL + ':' + d.SavePath + '.downloaded');

								PubSub.unsubscribe(d.URL + ':' + d.SavePath + '.progress');
								PubSub.unsubscribe(d.URL + ':' + d.SavePath + '.done');

								res(data);
						} else if (err) rej(err);else rej(new Error('Bad response code: ' + data.statusCode));
				}).on('data', function (chunk) {
						return PubSub.publish(d.URL + ':' + d.SavePath + '.progress', chunk.length);
				}).pipe(_fs2.default.createWriteStream(d.SavePath).on('error', rej));
		});
};

var headrequest = _bluebird2.default.promisify(_request2.default.head);

var Download = function () {
		function Download(url, path) {
				var _this = this;

				_classCallCheck(this, Download);

				this.URL = void 0;
				this.SavePath = void 0;
				this.Size = void 0;
				this.Downloaded = 0;

				this.URL = url;
				this.SavePath = path;

				PubSub.subscribe(this.URL + ':' + this.SavePath + '.progress', function (e, size) {

						if (typeof size != 'undefined') {

								_this.Downloaded += Number(size);

								PubSub.publish(_this.URL + ':' + _this.SavePath + '.done', [_this.Downloaded, _this.Size]);
						}
				});
		}

		_createClass(Download, [{
				key: 'getSize',
				value: function getSize() {
						var _this2 = this;

						return headrequest(this.URL).then(function (data) {
								return _this2.Size = Number(data.headers['content-length']);
						});
				}
		}, {
				key: 'start',
				value: function start(ngs) {
						var _this3 = this;

						return _bluebird2.default.all([new _bluebird2.default(function (res) {
								return PubSub.subscribe(_this3.URL + ':' + _this3.SavePath + '.downloaded', function (e) {
										res();PubSub.unsubscribe(e);
								});
						}), typeof ngs === 'undefined' ? this.getSize().then(prequest(this)) : prequest(this)]);
				}
		}]);

		return Download;
}();

exports.default = Download;