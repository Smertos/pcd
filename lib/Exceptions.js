'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Exception = function (_Error) {
	_inherits(Exception, _Error);

	function Exception() {
		var _Object$getPrototypeO;

		var _temp, _this, _ret;

		_classCallCheck(this, Exception);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_Object$getPrototypeO = Object.getPrototypeOf(Exception)).call.apply(_Object$getPrototypeO, [this].concat(args))), _this), _this.name = 'Exception', _temp), _possibleConstructorReturn(_this, _ret);
	}

	return Exception;
}(Error);

var TimeoutException = function (_Exception) {
	_inherits(TimeoutException, _Exception);

	function TimeoutException() {
		var _Object$getPrototypeO2;

		var _temp2, _this2, _ret2;

		_classCallCheck(this, TimeoutException);

		for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
			args[_key2] = arguments[_key2];
		}

		return _ret2 = (_temp2 = (_this2 = _possibleConstructorReturn(this, (_Object$getPrototypeO2 = Object.getPrototypeOf(TimeoutException)).call.apply(_Object$getPrototypeO2, [this].concat(args))), _this2), _this2.name = 'TimeoutException', _temp2), _possibleConstructorReturn(_this2, _ret2);
	}

	return TimeoutException;
}(Exception);

exports.default = { Exception: Exception, TimeoutException: TimeoutException };