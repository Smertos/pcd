# pcd - Promised Concurrent Downloader
### NPM module which target is to provide simple downloading API using Promises & PubSub


## Installation
```
npm install https://gitlab.com/Smertos/pcd.git --save
```

## Code Examples (ES2015)
#### Single file download:

```javascipt
import PubSub from 'pubsub-js'
import pcd from 'pcd'

let
    url = 'http://i.imgur.com/vhhIAje.png',
    path = 'C:\\Projects\\JS\\pcd\\img.png';

//Listen for progress/completion events
PubSub.subscribe(url + ':' + path + '.done', (e, data) => console.log('Downloaded: ' + data[0] + '/' + data[1]));
PubSub.subscribe(url + ':' + path + '.downloaded', (e, data) => console.log('Download complete'));

//Start promised download
pcd.download(url, path)
	.then(() => console.log('Downloaded!'))
	.catch(err => console.error(err.message));
```

#### Multiple file download:

```javascipt
import PubSub from 'pubsub-js'
import pcd from 'pcd'
import path from'path';

let downloads = [
  {URL: 'http://i.imgur.com/vhhIAje.png', path: 'img1.jpg'},
  {URL: 'http://i.imgur.com/vhhIAje.png', path: 'img2.jpg'},
  {URL: 'http://i.imgur.com/vhhIAje.png', path: 'img3.jpg'},
  {URL: 'http://i.imgur.com/vhhIAje.png', path: 'img4.jpg'},
  {URL: 'http://i.imgur.com/vhhIAje.png', path: 'img5.jpg'},
  {URL: 'http://i.imgur.com/vhhIAje.png', path: 'img6.jpg'},
  {URL: 'http://i.imgur.com/vhhIAje.png', path: 'img7.jpg'},
  {URL: 'http://i.imgur.com/vhhIAje.png', path: 'img8.jpg'}
];

downloads.forEach(el => PubSub.subscribe(el.URL + ':' + el.path + '.progress', (e, size) => {
	if(size != undefined)
		console.log('[' + path.basename(el.path) + '] Got ' + size + ' bytes');
}));

PubSub.subscribe('mega-download.progress', (e, obj) => console.log(obj.downloaded + '/' + obj.sizeOverall));

pcd.downloadAll(downloads, 'mega-download', 200, 2).catch((err) => console.log('Error: ' + err.message)).then(() => console.log('We\'re done there'));
```