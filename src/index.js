import path from 'path'
import excps from './Exceptions.js'
import Download from './Download.js'
import DownloadAll from './DownloadAll.js'

if(typeof global.PubSub == 'undefined')
  global.PubSub = require('pubsub-js');

export default {
  download: (URL, Path) => new Download(URL, Path).start() /*.catch((err) => console.log('Error: ' + err.message))*/ ,
  downloadAll: DownloadAll
}
