class Exception extends Error {
	name = 'Exception'
}

class TimeoutException extends Exception {
	name = 'TimeoutException'
}

export default {Exception, TimeoutException}