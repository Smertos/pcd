import Promise from 'bluebird'
import Download from './Download.js'

export default function DownloadAll(dArr, eventName, interval, concurrency) {

  let promises = dArr.map(el => new Download(el.URL, el.path));
  let downloaded = 0;
  let sizeOverall = 0;

  return Promise.map(promises, el => el.getSize(), {concurrency}).then(() => {
    promises.forEach(el => {
      PubSub.subscribe(el.URL + ':' + el.SavePath + '.progress', (e, size) => {
        if(typeof size != 'undefined')
          downloaded += Number(size);
      });

      sizeOverall += Number(el.Size);
    });

    let inter = setInterval(() => PubSub.publish(eventName + '.progress', {downloaded, sizeOverall}), interval);

    PubSub.subscribe(eventName + '.downloaded', () => clearInterval(inter));

    return Promise.map(promises, el => el.start(), {concurrency}).then(() => PubSub.publish(eventName + '.downloaded'));
  });
};
