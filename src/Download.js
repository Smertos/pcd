import fs from 'fs'
import path from 'path'

import Promise from 'bluebird'
import request from 'request'

import excps from './Exceptions.js'

let prequest = (d) =>
    new Promise(
		(res, rej) =>
			request.get(d.URL, (err, data) => {
				if(!err) {
					
					PubSub.publish(d.URL + ':' + d.SavePath + '.downloaded');
		  
					PubSub.unsubscribe(d.URL + ':' + d.SavePath + '.progress');
					PubSub.unsubscribe(d.URL + ':' + d.SavePath + '.done');
					
					
					res(data);
				}
				else if(err) rej(err);
				else rej(new Error('Bad response code: ' + data.statusCode))
			}).on('data', (chunk) => PubSub.publish(d.URL + ':' + d.SavePath + '.progress', chunk.length))
			.pipe(fs.createWriteStream(d.SavePath).on('error', rej))
    );

let headrequest = Promise.promisify(request.head);

export default class Download {
  URL = void(0)
  SavePath = void(0)
  Size = void(0)
  Downloaded = 0

  constructor(url, path) {
    this.URL = url;
    this.SavePath = path;

    PubSub.subscribe(this.URL + ':' + this.SavePath + '.progress', (e, size) => {
		
      if(typeof size != 'undefined') {
		  
        this.Downloaded += Number(size);
		
		PubSub.publish(this.URL + ':' + this.SavePath + '.done', [this.Downloaded, this.Size]);
	  
      }
    });
  }

  getSize() {
    return headrequest(this.URL).then((data) => this.Size = Number(data.headers['content-length']));
  }

  start(ngs) {
    return Promise
      .all(
		[
			new Promise(res => PubSub.subscribe(this.URL + ':' + this.SavePath + '.downloaded', e => { res(); PubSub.unsubscribe(e); })),
			(typeof ngs === 'undefined') ? this.getSize().then(prequest(this)) : prequest(this)
		]
	  );
  }


}
